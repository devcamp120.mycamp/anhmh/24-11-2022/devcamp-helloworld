export const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: 'https://reactjs.org/logo-og.png',
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100
}

export function countPercentStudyingStudents() {
    return gDevcampReact.studyingStudents / gDevcampReact.totalStudents * 100; 
}