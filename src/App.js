import {gDevcampReact, countPercentStudyingStudents} from './info';
import image from './assets/images/logo-og.png'

function App() {
  var sPercentStudyingStudents = countPercentStudyingStudents();

  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src={image} alt="Devcamp" width="500"/>
      <p>Tỷ lệ sinh viên đang theo học: {sPercentStudyingStudents}%</p>
      <p>{sPercentStudyingStudents >= 15 ? "Sinh viên theo học nhiều": "Sinh viên theo học ít"}</p>
      <ul>
        {gDevcampReact.benefits.map(function(pElement, pIndex) {
            return <li>{pElement}</li>;
          })}
      </ul>
    </div>
  );
}

export default App;
